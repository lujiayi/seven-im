package pers.lujiayi.im.constant;

/**
 * @author Lujiayi
 * @date 2020/5/15
 */
public interface Constant {

    String TYPE_USER = "user";

    String TYPE_GROUP = "group";

}
