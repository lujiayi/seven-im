package pers.lujiayi.im.websocket;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.BeanUtils;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import pers.lujiayi.im.entity.ImGroupUser;
import pers.lujiayi.im.entity.ImMessage;
import pers.lujiayi.im.entity.ImUser;
import pers.lujiayi.im.mapper.ImGroupUserMapper;
import pers.lujiayi.im.mapper.ImMessageMapper;
import pers.lujiayi.im.mapper.ImOfflineMessageMapper;
import pers.lujiayi.im.mapper.ImUserMapper;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@ServerEndpoint("/im/{id}")
@Component
public class WebSocketServer {

    private static Integer ONLINE = 0;

    public static ConcurrentHashMap<String, WebSocketServer> WEBSOCKET_MAP = new ConcurrentHashMap<>();

    private Session session;

    private String userId;

    private static ImGroupUserMapper imGroupUserMapper;

    private static ImMessageMapper imMessageMapper;

    private static ImUserMapper imUserMapper;

    private static ImOfflineMessageMapper imOfflineMessageMapper;

    public static void setApplicationContext(ConfigurableApplicationContext applicationContext) {
        WebSocketServer.imGroupUserMapper = applicationContext.getBean(ImGroupUserMapper.class);
        WebSocketServer.imMessageMapper = applicationContext.getBean(ImMessageMapper.class);
        WebSocketServer.imUserMapper = applicationContext.getBean(ImUserMapper.class);
        WebSocketServer.imOfflineMessageMapper = applicationContext.getBean(ImOfflineMessageMapper.class);
    }

    private static ImMessage robot(ImMessage imMessage) {
        String response = HttpUtil.get("http://api.qingyunke.com/api.php?key=free&appid=0&msg=" + imMessage.getContent());
        JSONObject jsonObject = JSON.parseObject(response);
        String content = (String) jsonObject.get("content");
        ImMessage robotImMessage = imMessage;
        robotImMessage.setFromId("10000");
        robotImMessage.setFromName("小菲菲");
        robotImMessage.setFromAvatar("avatar.jpg");
        robotImMessage.setContent(content);
        imMessage.setSendTime(LocalDateTime.now());
        return imMessage;
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("id") String id) {
        this.userId = id;
        this.session = session;
        WebSocketServer webSocketServer = WEBSOCKET_MAP.get(this.userId);
        if (webSocketServer == null) {
            WEBSOCKET_MAP.put(this.userId, this);
        } else {
            ImMessage imMessage = new ImMessage();
            imMessage.setMsgType(ImMessage.TYPE_SYSTEM);
            imMessage.setContent(ImMessage.MSG_TYPE_FORCE_LEAVE);
            imMessage.setToId(this.userId);
            imMessage.setSendTime(LocalDateTime.now());
            Future<Void> future = WebSocketServer.send2One(this.userId, imMessage);
            if (future != null && future.isDone()) {
                WEBSOCKET_MAP.put(this.userId, this);
            }
        }
        ImUser imUser = new ImUser();
        imUser.setId(id);
        imUser.setStatus(ImUser.ONLINE);
        imUserMapper.updateById(imUser);
    }

    @OnMessage
    public void onMessage(String message) {
        ImMessage imMessage = JSON.parseObject(message, ImMessage.class);
        imMessage.setSendTime(LocalDateTime.now());
        imMessage.setId(IdUtil.simpleUUID());
        imMessageMapper.insert(imMessage);
        if (imMessage.getType().equals(ImMessage.TYPE_SYSTEM)) {
            if (imMessage.getMsgType().equals(ImMessage.MSG_TYPE_JOIN)) {
                WebSocketServer.send2Group(this.getUserByGroupId(imMessage.getToId()), imMessage);
            }
        } else if (imMessage.getType().equals(ImMessage.TYPE_GROUP)) {
            WebSocketServer.send2Group(this.getUserByGroupId(imMessage.getToId()), imMessage);
            if (imMessage.getToId().equals("1") && !(imMessage.getContent().startsWith("[IMG]") || imMessage.getContent().startsWith("[FILE]"))) { // 公共群机器人回复
                ImMessage robot = WebSocketServer.robot(imMessage);
                ImMessage robotImMessage = new ImMessage();
                robot.setId(IdUtil.simpleUUID());
                BeanUtils.copyProperties(robot, robotImMessage);
                imMessageMapper.insert(robotImMessage);
                WebSocketServer.send2Group(this.getUserByGroupId(robot.getToId()), robot);
            }
        } else {
            WebSocketServer.send2One(imMessage.getFromId(), imMessage);
            WebSocketServer.send2One(imMessage.getToId(), imMessage);
        }

    }

    @OnClose
    public void onClose() {
        WEBSOCKET_MAP.remove(this.userId);
        // 用户下线
        ImUser imUser = imUserMapper.selectById(this.userId);
        imUser.setStatus(ImUser.OFFLINE);
        imUserMapper.updateById(imUser);
        // 发送用户下线广播
        ImMessage imMessage = new ImMessage();
        imMessage.setFromId(imUser.getId());
        imMessage.setFromName(imUser.getName());
        imMessage.setFromAvatar(imUser.getAvatar());
        imMessage.setType(ImMessage.TYPE_SYSTEM);
        imMessage.setMsgType(ImMessage.MSG_TYPE_LEAVE);
        imMessage.setContent(imUser.getName() + " 离开了");
        imMessage.setSendTime(LocalDateTime.now());
        LambdaQueryWrapper<ImGroupUser> query = new LambdaQueryWrapper();
        query.eq(ImGroupUser::getUserId, this.userId);
        List<ImGroupUser> imGroupUsers = this.imGroupUserMapper.selectList(query);
        if (CollUtil.isNotEmpty(imGroupUsers)) {
            for (ImGroupUser imGroupUser : imGroupUsers) {
                imMessage.setToId(imGroupUser.getGroupId());
                WebSocketServer.send2Group(this.getUserByGroupId(imGroupUser.getGroupId()), imMessage);
            }
        }
    }

    public List<String> getUserByGroupId(String groupId) {
        return this.imGroupUserMapper.selectList(new LambdaQueryWrapper<ImGroupUser>().eq(ImGroupUser::getGroupId, groupId)).stream().map(ImGroupUser::getUserId).collect(Collectors.toList());
    }

    public static void send2Group(List<String> userIds, ImMessage imMessage) {
        for (String userId : userIds) {
            WebSocketServer.send2One(userId, imMessage);
        }
    }

    public static Future<Void> send2One(String userId, ImMessage imMessage) {
        WebSocketServer webSocketServer = WEBSOCKET_MAP.get(userId);
        if (webSocketServer == null) {
            //TODO 存放离线信息
//            ImOfflineMessage imOfflineMessage = new ImOfflineMessage();
//            imOfflineMessage.setId(IdUtil.simpleUUID());
//            imOfflineMessage.setUserId(userId);
//            imOfflineMessage.setMessageId(imMessageDTO.getId());
//            imOfflineMessageMapper.insert(imOfflineMessage);
        } else {
            return webSocketServer.session.getAsyncRemote().sendText(JSON.toJSONString(imMessage));
        }
        return null;
    }
}
