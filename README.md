# seven-im
之前发布的文章 [java-websocket即时通讯-聊天室](https://blog.csdn.net/qq_29485177/article/details/78667219) 因为年代久远，代码找不到了...，有的小伙伴留言需要源码，所以就另写了im...，文章末尾有演示地址及源码
#### 效果

> 基本

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200430163435105.gif)

> 表情

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020043016353645.gif)

> 图片&文件

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200430163604173.gif)

> 多会话，私聊（诺克打字有点慢...）

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200430163649361.gif)

## 移动端

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200721160317644.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI5NDg1MTc3,size_16,color_FFFFFF,t_70)


#### 功能
 - [x] 聊天室
 - [x] 群聊
 - [x] 私聊
 - [x] 表情&图片&文件
 - [ ] 聊天记录
 - [ ] 好友分类
 - [ ] 离线消息
 - [ ] 导入用户
 - [ ] 多端在线
 - [x] pc版
 - [x] 移动版(vue)
 - [ ] 前端vue版

#### 技术
PC版前端：jquery

移动版前端： vue、vant

后端：springboot、websocket、mybatis-plus

PC演示地址： ~~欠费了~~

gitee：[https://gitee.com/lujiayi/seven-im](https://gitee.com/lujiayi/seven-im)

 